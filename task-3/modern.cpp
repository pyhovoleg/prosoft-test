#include <map>
#include <iostream>

#define LEGACY_SIZE 3
extern int values[LEGACY_SIZE];

class MyBlah
{
public:
    MyBlah(int i) : _value(i) {};
    int getValue() { return _value; }
private:
    int _value;
};

class Adapter
{
public:
    Adapter()
    {
        for (int i = 0; i < LEGACY_SIZE; ++i)
    	{
        	map_[values[i]] = new MyBlah(values[i]);
            std::cout << map_.at(values[i])->getValue() << std::endl;
        }
    }
    ~Adapter()
    {
        for (int i = 0; i < LEGACY_SIZE; ++i)
            delete map_[values[i]];
    }
private:
    std::map<int, MyBlah *> map_;
};

int main()
{
    Adapter adapter;
}