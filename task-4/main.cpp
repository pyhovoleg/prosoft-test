#include <stdio.h>

typedef struct
{
    double x;
    double y;
} point_t;

// interface class
class FeatureTypeStrategy
{
public:
    virtual bool read(FILE* file) = 0;
    virtual void draw() = 0;
};

// cocnrete interfaces
class CircleStrategy : public FeatureTypeStrategy
{
public:
    bool read(FILE* file) override
    {
        return fread(&_geometry, sizeof(_geometry), 1, file) == 1;
    }
    void draw() override {}
private:
    struct
    {
        point_t center;
        double radius;
    } _geometry;
};

class PolygonStrategy : public FeatureTypeStrategy
{
public:
    PolygonStrategy(unsigned int apexNumber) : _apexNumber(apexNumber)
                                             , _apexes(nullptr)
    {
        _apexes = new point_t[_apexNumber];
    }
    ~PolygonStrategy()
    {
        delete[] _apexes;
    }
    bool read(FILE* file) override
    {
        return fread(_apexes, sizeof(*_apexes), _apexNumber, file) == _apexNumber;
    }
    void draw() override {}
private:
    unsigned int _apexNumber;
    point_t* _apexes;
};

// context class
class Feature
{
public:
    enum FeatureType
    {
        eUnknown,
        eCircle,
        eTriangle,
        eSquare
    };

    Feature() : _type(eUnknown) {}
    ~Feature()
    {
        _deleteStrategy();
    }

    bool read(FILE* file)
    {
        _deleteStrategy();

        if (fread(&_type, sizeof(FeatureType), 1, file) != 1)
            return false;

        switch (_type)
        {
        case eCircle:
            _strategy = new CircleStrategy;
            break;
        case eTriangle:
            _strategy = new PolygonStrategy(3);
            break;
        case eSquare:
            _strategy = new PolygonStrategy(4);
            break;
        default:
            break;
        }

        return _strategy->read(file);
    }

    void draw()
    {
        _strategy->draw();
    }

    bool isValid()
    {
        return _type != eUnknown;
    }
    
private:

    void _deleteStrategy()
    {
        if (_strategy)
        {
            if (CircleStrategy* circle = dynamic_cast<CircleStrategy*>(_strategy))
                delete circle;
            else if (PolygonStrategy* polygon = dynamic_cast<PolygonStrategy*>(_strategy))
                delete polygon;
        }
    }

    FeatureTypeStrategy* _strategy;
    FeatureType _type;
};

int main()
{
    Feature::FeatureType type = Feature::eSquare;
    double points[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

    FILE* file = fopen("task-4/features.dat", "wb");
    fwrite(&type, sizeof(type), 1, file);
    fwrite(points, sizeof(points), 1, file);
    fclose(file);

    Feature feature;
    file = fopen("task-4/features.dat","rb");

    if (!feature.read(file))
        return 1;

    fclose(file);

    if (!feature.isValid())
        return 1;

    return 0;
}