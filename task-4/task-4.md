Что не так в этом коде? Перечислите, какие недостатки вы видите. Предложите свой вариант рефакторинга.
``` cpp
#include <stdio.h>
 
class Feature
{
public:
    enum FeatureType {eUnknown, eCircle, eTriangle, eSquare};
 
    Feature() : type(eUnknown), points(0) {    }
 
    ~Feature()
    {
        if (points)
            delete points;
    }
 
    bool isValid() 
    {
        return type != eUnknown;
    }
 
    bool read(FILE* file)
    {        
        if (fread(&type, sizeof(FeatureType), 1, file) != sizeof(FeatureType))
            return false;
        short n = 0;
        switch (type) 
        {
        case eCircle: n = 3; break;
        case eTriangle:    n = 6; break;
        case eSquare: n = 8; break;
        default: type = eUnknown; return false;
        }
        points = new double[n];
        if (!points)
            return false;
        return fread(&points, sizeof(double), n, file) == n*sizeof(double);
    }
    void draw()
    {
        switch (type)
        {
        case eCircle: drawCircle(points[0], points[1], points[2]); break;
        case eTriangle:    drawPoligon(points, 6); break;
        case eSquare: drawPoligon(points, 8); break;
        }
    }
 
protected:
    void drawCircle(double centerX, double centerY, double radius);
    void drawPoligon(double* points, int size);
 
    double* points;
    FeatureType type;        
};
 
int main(int argc, char* argv[])
{
    Feature feature;
    FILE* file = fopen("features.dat", "r");
    feature.read(file);
    if (!feature.isValid())
        return 1;
    return 0;
}
```
Проблемы:
1. в каждом методе приходится писать switch-case для каждой фигуры;
2. массив, на который указывает `points` по разному интерпретируется в методах `drawCircle` и `drawPoligon`, это ухудшает ясность кода.

Баги:
1. `fread` возвращает не размер считанных данных в байтах, а число считанных элменетов: `if (fread(&type, sizeof(FeatureType), 1, file) != 1)` и `return fread(&points, sizeof(double), n, file) == n);`;
4. в строке `return fread(&points, sizeof(double), n, file) == n*sizeof(double);` перед `points` знак амперсанда `&` не нужен, это и так указатель.

Замечания:
1. в вызовах `fread` размер считываемой единицы информации лучше писать не `sizeof(тип)`, а `sizeof(первый элемент массива)`;
2. в списке инициализации `Feature()` инициализировать `points` нужно не `0`, а `nullptr`.

Задача вновь была решена с использованием паттерна Стратегия [main.cpp](main.cpp). Для каждого вида фигур были созданые интерфейсные классы "стратегии". При этом для многоугольника класс один - в нем нет конструктора по умолчанию и есть поле для количества вершин многоугольника. При выполнении задачи я понимал, что по-хорошему для квадрата (правильного четырехугольника) надо организовать отдельную логику, но чтобы не усложнять чрезмерно код, ответственность за "квадратность" я переложил на читаемый бинарник.

На моей платформе проблем с выравниванием в структуре `CircleStrategy::_geometry` и массиве `PolygonStrategy::_points` при использовании их в качестве буфера в вызовах `fread` не было. Можно добавить `__attribute__((packed))` к каждой структуре во избежание таких проблем.