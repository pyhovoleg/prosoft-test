#include "generic.h"

int main()
{ 
    uchar uchar_array[5] = {1, 2, 2, 3, 4};
    if (linear_search_uchar(uchar_array, 5, 2) != uchar_array + 1)
        return 1;

    float float_array[3] = {1., 3.4, 6.2};
    if (linear_search_float(float_array, 3, 6.2) != float_array + 2)
        return 1;

    int int_array[10] = {0};
    if (linear_search_int(int_array, 10, 1) != NULL)
        return 1;
    
    return 0;
}