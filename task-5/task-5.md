Имеем чистый C. Напишите generic функцию линейного поиска в массиве. И приведите пример использования вашей функции.

Идея решения была взята со статье на хабре "[Шаблоны на C. Да! На чистом С. Не С++](https://habr.com/ru/post/154811/)". Для имитации шаблонной функции используется object-like макрос `T`, который и определяет тип передаваемых и возвращаемого параметров, и входит в состав лексемы функции.
```c
#ifdef T

/* generic linear search */
const T* CAT(linear_search, T)(const T* array, size_t count, const T value)
{
    for (size_t i = 0; i < count; i++)
    {
        if (array[i] == value)
            return &array[i];
    }
        
    return NULL;
}
```

Для включения его в состав лексемы используется макрос конкатенации `CAT(X, Y)` с "помощником" `CAT_HELPER(X, Y)` на случай использования в качестве параметра этого function-like макроса другого макроса (у нас это `T`) ([Advanced preprocessor tips and tricks](https://www.iar.com/support/resources/articles/advanced-preprocessor-tips-and-tricks/)).
```c
#define CAT_HELPER(X,Y) X##_##Y
#define CAT(X, Y) CAT_HELPER(X, Y)
```
Файл [generic-realization.h](generic-realization.h) содержит отпределения этих макросов и generic-функции. В файле [generic.h](generic.h), который должен включаться пользователем в проект, содержится множественное переопределение макроса `T` и включение нашей обощенной функции. Вот так выглядит обработанный препроцессором исходный код, определяющий обощенную функцию для типов `unsigned int` и `long`:
```c
#define T uint
# 1 "task-5/generic-realization.h" 1
# 16 "task-5/generic-realization.h"
const uint* linear_search_uint(const uint* array, size_t count, const uint value)
{
    for (size_t i = 0; i < count; i++)
    {
        if (array[i] == value)
            return &array[i];
    }

    return 
# 24 "task-5/generic-realization.h" 3 4
          ((void *)0)
# 24 "task-5/generic-realization.h"
              ;
}
# 34 "task-5/generic.h" 2
#undef T

#define T long
# 1 "task-5/generic-realization.h" 1
# 16 "task-5/generic-realization.h"
const long* linear_search_long(const long* array, size_t count, const long value)
{
    for (size_t i = 0; i < count; i++)
    {
        if (array[i] == value)
            return &array[i];
    }

    return 
# 24 "task-5/generic-realization.h" 3 4
          ((void *)0)
# 24 "task-5/generic-realization.h"
              ;
}
# 38 "task-5/generic.h" 2
#undef T
```