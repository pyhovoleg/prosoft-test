#ifndef _GENERIC_H_
#define _GENERIC_H_

typedef unsigned char uchar;
typedef unsigned short ushort;
typedef unsigned int uint;
typedef unsigned long ulong;
typedef long long longlong;
typedef unsigned long long ulonglong;
typedef long double longdouble;

#define T char
#include "generic-realization.h"
#undef T

#define T uchar
#include "generic-realization.h"
#undef T

#define T short
#include "generic-realization.h"
#undef T

#define T ushort
#include "generic-realization.h"
#undef T

#define T int
#include "generic-realization.h"
#undef T

#define T uint
#include "generic-realization.h"
#undef T

#define T long
#include "generic-realization.h"
#undef T

#define T ulong
#include "generic-realization.h"
#undef T

#define T longlong
#include "generic-realization.h"
#undef T

#define T ulonglong
#include "generic-realization.h"
#undef T

#define T float
#include "generic-realization.h"
#undef T

#define T double
#include "generic-realization.h"
#undef T

#define T longdouble
#include "generic-realization.h"
#undef T

#endif /* _GENERIC_H_ */