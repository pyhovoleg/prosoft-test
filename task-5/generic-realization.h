/*- concatenation macro, it's helper and also std definitions header aren't --*/
/*------------------- about to be included more than once --------------------*/
#ifndef CAT

#include <stddef.h>

#define CAT_HELPER(X,Y) X##_##Y
#define CAT(X, Y) CAT_HELPER(X, Y)

#endif /* CAT */

/*--------------------- C-generic functions definitions ----------------------*/
#ifdef T

/* generic linear search */
const T* CAT(linear_search, T)(const T* array, size_t count, const T value)
{
    for (size_t i = 0; i < count; i++)
    {
        if (array[i] == value)
            return &array[i];
    }
        
    return NULL;
}

/* other generic functions ... */

#endif /* T */