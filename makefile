PHONY: clean

CPP_COMP = g++
C_COMP = gcc
FLAGS = -g3

task-6: task-6/stack.c task-6/main.c
	@$(C_COMP) $(FLAGS) -c task-6/stack.c -o stack.o
	@$(C_COMP) $(FLAGS) -c task-6/main.c -o main.o
	@$(C_COMP) $(FLAGS) stack.o main.o -o main

task-5: task-5/main.c
	@$(C_COMP) $(FLAGS) -c $^ -o main.o
	@$(C_COMP) $(FLAGS) -E $^ -o main.i
	@$(C_COMP) $(FLAGS) main.o -o main

task-3: task-3/legacy.c task-3/modern.cpp
	@$(C_COMP) $(FLAGS) -c task-3/legacy.c -o legacy.o
	@$(CPP_COMP) $(FLAGS) -c task-3/modern.cpp -o modern.o
	@$(CPP_COMP) $(FLAGS) legacy.o modern.o -o main

task-%: task-%/main.cpp
	@$(CPP_COMP) $(FLAGS) -c $^ -o main.o
	@$(CPP_COMP) $(FLAGS) main.o -o main

clean:
	@rm -r -f main *.o *.i