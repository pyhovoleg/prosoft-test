Есть класс CodeGenerator, который умеет генерить код на разных языках.
Предложите рефакторинг с учетом, что количество языков будет расширяться
``` cpp
class CodeGenerator
{
public:
    enum Lang {JAVA, C_PLUS_PLUS, PHP};
    CodeGenerator(Lang language) { _language=language; }
    std::string generateCode()
    {
        switch(_language) {
        case JAVA:        //return generated java code
        case C_PLUS_PLUS: //return generated C++ code
        case PHP:         //return generated PHP code
        }
        throw new std::logic_error("Bad language");
    }
    std::string someCodeRelatedThing() // used in generateCode()
    {
        switch(_language) {
        case JAVA:        //return generated java-related stuff
        case C_PLUS_PLUS: //return generated C++-related stuff
        case PHP:         //return generated PHP-related stuff
        }
        throw new std::logic_error("Bad language");
    }

private:
    Lang _language;
}
```

Потенциальные проблемы кода на листинге выше:
1. дублирование switch-case в каждом методе класса, чувствительном к выбору языка (т.е., учитывая специфику класса, практически в любом);
2. необходимость помещать реализацию алгоритма прямо в блок switch-case (приводит к чрезмерному раздуванию кода метода) или осуществлять перенаправление вызовов (засорение класса множеством похожих методов);
3. необходимость в каждом методе проверять валидность используемого языка и писать вызов исключения;
4. сочетание всех вышеперечисленных проблем и необходимости добавить еще один язык приводит к раздуванию объема исходника и усложнению кода.

Эти проблемы можно решить с помощью шаблона проектирования "[Стратегия](https://refactoring.guru/ru/design-patterns/strategy)". Реализация паттерна (с некоторыми модификациями - в файле [main.cpp](/task-2/main.cpp)).

Для каждого языка должен создаваться класс конкретной стратегии (интерфейса), унаследованный от класса абстрактного `LanguageStrategy` и переопределяющий его методы.

Пример:
``` cpp
class RustStrategy : public LanguageStrategy
{
public:
    ~RustStrategy() { std::cout << "Bye Rust!\n"; };
    std::string generateCode() override { return "Hello Rust!\n"; };
    std::string someCodeRelatedThing() override {};
};
```

Класс `CodeGenerator`, в паттерне "Стратегия" именуемый контекстом, немного отличается от классической реализации контекста. В нем отсутствует возможность изменения стратегии, которая определяется в конструкторе.

Для избежания утечки необходимо, чтобы вызывался деструктор соответсвующей конктретной стратегии, а не только `~LanguageStrategy()`. Для этого было использован `dynamic_cast`. Измененная реализация `CodeGenerator`:
``` cpp
// context class
class CodeGenerator
{
public:
    enum Lang {JAVA, C_PLUS_PLUS, PHP, RUST};
    CodeGenerator(Lang language)
    {
        switch (language)
        {
        case JAVA:
            _langStrategy = new JavaStrategy;
            break;
        case C_PLUS_PLUS:
            _langStrategy = new CppStrategy;
            break;
        case PHP:
            _langStrategy = new PhpStrategy;
            break;
        case RUST:
            _langStrategy = new RustStrategy;
            break;
        default:
            throw new std::logic_error("Bad language");
            break;
        }
    }
    ~CodeGenerator()
    {
        if (JavaStrategy* java = dynamic_cast<JavaStrategy*>(_langStrategy))
        {
            delete java;
        }
        else if (CppStrategy* cpp = dynamic_cast<CppStrategy*>(_langStrategy))
        {
            delete cpp;
        }
        else if (PhpStrategy* php = dynamic_cast<PhpStrategy*>(_langStrategy))
        {
            delete php;
        }
        else if (RustStrategy* rust = dynamic_cast<RustStrategy*>(_langStrategy))
        {
            delete rust;
        }
    }
    std::string generateCode()
    {
        return _langStrategy->generateCode();
    }
    std::string someCodeRelatedThing()
    {
        return _langStrategy->someCodeRelatedThing();
    }
private:
    LanguageStrategy* _langStrategy;
};
```
Для добавления языка Rust, помимо создания класса, потребовалось:
1. добавить одно перечисление в `Lang`;
2. добавить один блок `case` в конструторе:
``` cpp
case RUST:
    _langStrategy = new RustStrategy;
    break;
```
3. добавить одно условие в деструктор:
``` cpp
else if (RustStrategy* rust = dynamic_cast<RustStrategy*>(_langStrategy))
{
    delete rust;
}
```
UPD. Здесь и в четвертом задании под рефакторингом понималось переосмысление архитектуры без изменения интерфейсов классов вообще. За сим и понадобился паттерн. Если бы я предположил свободу действий, изменения были бы значительно проще - простое наследование и использование разных объектов для разных языков.
