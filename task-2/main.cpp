#include <stdexcept>
#include <string>
#include <iostream>

// interface class
class LanguageStrategy
{
public:
    virtual std::string generateCode() = 0;
    virtual std::string someCodeRelatedThing() = 0;
};

// concrete interfaces
class JavaStrategy : public LanguageStrategy
{
public:
    ~JavaStrategy() { std::cout << "Bye Java!\n"; };
    std::string generateCode() override { return "Hello Java!\n"; };
    std::string someCodeRelatedThing() override {};
};

class CppStrategy : public LanguageStrategy
{
public:
    ~CppStrategy() { std::cout << "Bye C++!\n"; };
    std::string generateCode() override { return "Hello C++!\n"; };
    std::string someCodeRelatedThing() override {};
};

class PhpStrategy : public LanguageStrategy
{
public:
    ~PhpStrategy() { std::cout << "Bye PHP!\n"; };
    std::string generateCode() override { return "Hello PHP!\n"; };
    std::string someCodeRelatedThing() override {};
};

class RustStrategy : public LanguageStrategy
{
public:
    ~RustStrategy() { std::cout << "Bye Rust!\n"; };
    std::string generateCode() override { return "Hello Rust!\n"; };
    std::string someCodeRelatedThing() override {};
};

// context class
class CodeGenerator
{
public:
    enum Lang {JAVA, C_PLUS_PLUS, PHP, RUST};
    CodeGenerator(Lang language)
    {
        switch (language)
        {
        case JAVA:
            _langStrategy = new JavaStrategy;
            break;
        case C_PLUS_PLUS:
            _langStrategy = new CppStrategy;
            break;
        case PHP:
            _langStrategy = new PhpStrategy;
            break;
        case RUST:
            _langStrategy = new RustStrategy;
            break;
        default:
            throw new std::logic_error("Bad language");
            break;
        }
    }
    ~CodeGenerator()
    {
        if (JavaStrategy* java = dynamic_cast<JavaStrategy*>(_langStrategy))
            delete java;
        else if (CppStrategy* cpp = dynamic_cast<CppStrategy*>(_langStrategy))
            delete cpp;
        else if (PhpStrategy* php = dynamic_cast<PhpStrategy*>(_langStrategy))
            delete php;
        else if (RustStrategy* rust = dynamic_cast<RustStrategy*>(_langStrategy))
            delete rust;
    }
    std::string generateCode()
    {
        return _langStrategy->generateCode();
    }
    std::string someCodeRelatedThing()
    {
        return _langStrategy->someCodeRelatedThing();
    }
private:
    LanguageStrategy* _langStrategy;
};

int main()
{
    CodeGenerator java(CodeGenerator::Lang::JAVA);
    std::cout << java.generateCode();

    CodeGenerator rust(CodeGenerator::Lang::RUST);
    std::cout << rust.generateCode();
    
    return 0;
}