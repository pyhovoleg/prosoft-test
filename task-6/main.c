#include <stdio.h>
#include <string.h>
#include "stack.h"

typedef enum
{
    FAIR,
    BITTER,
    MERRY,
    VILE,
    DUMB,
    MYSTERY
} temper_t;

typedef struct
{
    const char* fullname;
    int age;
    temper_t temper;
} person_t;

void print_guys(const stack_t* stk);
int compare_guys(const void* struct_1, const void* struct_2);

int main()
{
    person_t lana = {"Lana Del Rey", 35, BITTER};
    person_t tom = {"Lord Voldemort", 72, VILE};
    person_t john = {"John Doe", 0, MYSTERY};
    person_t merry = {"Meriadoc Brandybuck", 37, MERRY};

    // fill stack
    stack_t* guys = NULL;
    stack_push(&guys, &lana, sizeof(lana));
    stack_push(&guys, &tom, sizeof(tom));
    stack_push(&guys, &john, sizeof(john));
    stack_push(&guys, &merry, sizeof(merry));

    // print stack
    stack_print(guys, print_guys);

    // check stack size
    printf("\nStack size is %li elements\n", stack_size(guys));

    // pop merry from stack and check if this is merry
    person_t merry_pop = {"\0", 0, MYSTERY};
    stack_pop(&guys, &merry_pop, sizeof(merry_pop));
    if ( compare_guys(&merry, &merry_pop) )
        printf("\nMerry popped successfully!\n");

    stack_print(guys, print_guys);
    printf("\nStack size is %li elements\n", stack_size(guys));

    // let's find John Doe!
    const stack_t* john_node = stack_find(guys, compare_guys, &john);
    if ( compare_guys(john_node->data, &john) )
        printf("\nNow! John Doe is found!\n");

    // free guys
    stack_free(&guys);
    stack_print(guys, print_guys);

    return 0;
}

void print_guys(const stack_t* stk)
{
    person_t* guy = (person_t*)stk->data;
    printf("\nName: %s\nAge: %i\n", guy->fullname, guy->age);

    char* temper;
    switch (guy->temper)
    {
    case FAIR:      temper = "fair";    break;
    case BITTER:    temper = "bitter";  break;
    case MERRY:     temper = "merry";   break;
    case VILE:      temper = "vile";    break;
    case DUMB:      temper = "dumb";    break;
    case MYSTERY:   temper = "N/A";     break;
    default: break;
    }

    printf("Temper: %s\n", temper);
}

int compare_guys(const void* struct_1, const void* struct_2)
{
    const person_t* guy_1 = (const person_t*)struct_1;
    const person_t* guy_2 = (const person_t*)struct_2;

    return ( strcmp( guy_1->fullname, guy_2->fullname ) == 0 ) &&
           ( guy_1->age     == guy_2->age )                    &&
           ( guy_1->temper  == guy_2->temper );
}