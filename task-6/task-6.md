Имеем чистый С. Напишите reusable API для работы со связным списком структур. Структуры могу быть разного типа, но в одном списке будут структуры одного типа. Приведите пример использования вашего API.

Идиентичную задачу решал в качестве домашнего задания в Школе Разработчиков. Решение взял оттуда с несколькими модификациями. Список методов для работы со списком (входящих в состав API) представлен в файле [stack.h](stack.h) 
```c
int stack_push(stack_t** stk, const void* data, size_t size);
int stack_pop(stack_t** stk, void* data, size_t size);
int stack_top(const stack_t* stk, void* data, size_t size);
size_t stack_size(const stack_t* stk);
void stack_free(stack_t** stk);
void stack_print(const stack_t* stk, stack_print_cb_t print_cb);
const stack_t* stack_find(const stack_t* stk, stack_compare_cb_t comp_cb, const void* data);
```
Описание этих методов дается в качестве doxygen-like комментариев в [stack.c](stack.c):
```c
/**
 * @brief Searches for the first element in stack equal with the given
 * 
 * @param stk pointer to the last stack node
 * @param max_cb callback function that compares stack elements
 * @param data element to be found
 * @return const stack_t* pointer to the stack node where the element were found, NULL if element
 *         weren't found
 */
const stack_t* stack_find(const stack_t* stk, stack_compare_cb_t comp_cb, const void* data)
{
    while (stk)
    {
        if(comp_cb(stk->data, data))
            break;

        stk = stk->prev;
    }
    
    return stk;
}
```
Демонстрация работы с ними - в теле `main()`. API реализует основные инструменты работы с односвязным списком, реализованным как стек. Все методы, опирающиеся на априорное знание типа данных в списке, принимают callback-функции в качестве аргументов.