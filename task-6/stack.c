#include "string.h"
#include "stack.h"
#include "stdio.h"

static void stack_free_last(stack_t** stk);

/**
 * @brief Pushes the element to the stack
 * 
 * @param stk pointer to the last stack node pointer
 * @param data pointer to data to be pushed
 * @param size size of data to be pushed
 * @return int stack condition
 */
int stack_push(stack_t** stk, const void* data, size_t size)
{
    if (!stk || !data || !size)
        return STK_WRONG_ARG;

    // allocate memory for new stack_t struct anbd check if malloc succeeded
    stack_t* head = (stack_t*)malloc(sizeof(stack_t));

    if (!head)
        return STK_MALLOC_FAILED;

    // allocate memory for data and check if malloc succeeded
    head->data = malloc(size);

    if (!head->data)
    {
        free(head);
        return STK_MALLOC_FAILED;
    }

    memcpy(head->data, data, size);

    head->size = size;

    // link new element with previous one
    head->prev = *stk;

    // move stack head
    *stk = head;

    return STK_OK;
}

/**
 * @brief Gives the top element of the stack
 * 
 * @param stk pointer to the last stack node
 * @param data pointer to memory where top element data should be stored
 * @param size size of data to be pushed
 * @return int stack condition
 */
int stack_top(const stack_t* stk, void* data, size_t size)
{
    if (!stk || !data)
        return STK_WRONG_ARG;
      
    if (stk->size > size)
        return STK_NOT_ENOUGH_DATA;
      
    memcpy(data, stk->data, stk->size);

    return STK_OK;
}

/**
 * @brief Pops last element from stack
 * 
 * @param stk pointer to the last stack node pointer
 * @param data pointer to memory where top element data should be stored
 * @param size size of data to be pushed
 * @return int stack condition
 */
int stack_pop(stack_t** stk, void* data, size_t size)
{
    if (!stk)
        return STK_WRONG_ARG;
  
    int result = stack_top(*stk, data, size);

    if (result == STK_OK)
        stack_free_last(stk);

    return result;
}

/**
 * @brief Returns number of elements in stack
 * 
 * @param stk pointer to the last stack node
 * @return size_t number of elements in stack
 */
size_t stack_size(const stack_t* stk) 
{
    size_t counter = 0;

    while (stk)
    {
        counter++;
        stk = stk->prev;
    }
    
    return counter;
}

/**
 * @brief Prints all elements of stack
 * 
 * @param stk pointer to the last stack node
 * @param print_cb callback function that prints data
 */
void stack_print(const stack_t* stk, stack_print_cb_t print_cb)
{
    printf("\nStack:\n");

    while (stk)
    {
        print_cb(stk);
        stk = stk->prev;
    }
}

/**
 * @brief Pops all elements from stack
 * 
 * @param stk pointer to the last stack node
 */
void stack_free(stack_t** stk)
{
    while (*stk)
        stack_free_last(stk);
}

/**
 * @brief Searches for the first element in stack equal with the given
 * 
 * @param stk pointer to the last stack node
 * @param max_cb callback function that compares stack elements
 * @param data element to be found
 * @return const stack_t* pointer to the stack node where the element were found, NULL if element
 *         weren't found
 */
const stack_t* stack_find(const stack_t* stk, stack_compare_cb_t comp_cb, const void* data)
{
    while (stk)
    {
        if(comp_cb(stk->data, data))
            break;

        stk = stk->prev;
    }
    
    return stk;
}

static void stack_free_last(stack_t** stk)
{
    // save pointer to previous element
    stack_t* head = (*stk)->prev;

    free((*stk)->data);
    free((*stk));

    // move stack head
    *stk = head;
}