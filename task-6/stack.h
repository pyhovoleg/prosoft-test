#include "stdlib.h"

#ifndef _STACK_H_
#define _STACK_H_

/* stack node */
typedef struct stack_
{
    void*           data;
    size_t          size;
    struct stack_*  prev;
} stack_t;

/* stack conditions */
enum
{
    STK_OK              = 0,
    STK_WRONG_ARG       = 1,
    STK_NOT_ENOUGH_DATA = 2,
    STK_MALLOC_FAILED   = 3 
};

/* callback typedefs */
typedef void (*stack_print_cb_t)(const stack_t* stk);
typedef int (*stack_compare_cb_t)(const void* struct_1, const void* struct_2);

/* API */
int stack_push(stack_t** stk, const void* data, size_t size);
int stack_pop(stack_t** stk, void* data, size_t size);
int stack_top(const stack_t* stk, void* data, size_t size);
size_t stack_size(const stack_t* stk);
void stack_free(stack_t** stk);
void stack_print(const stack_t* stk, stack_print_cb_t print_cb);
const stack_t* stack_find(const stack_t* stk, stack_compare_cb_t comp_cb, const void* data);

#endif /* _STACK_H_ */